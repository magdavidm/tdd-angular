import { Component }       from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';

import { DashboardComponent } from './dashboard.component';
import { ViewerComponent } from './viewer.component';

@Component({
  selector: 'my-app',
  template: `
    <h1>{{title}}</h1>
    <nav>
      <a [routerLink]="['/dashboard']">Dashboard</a>
      <a [routerLink]="['/viewer']">Viewer</a>
    </nav>
    <router-outlet></router-outlet>
  `,
  directives: [ROUTER_DIRECTIVES],
  providers: [
  ],
  styles: ['nav a { padding: 2px; text-decoration: none; border-radius: 2px; background-color: gainsboro; }'],
  // styleUrls: ['../../css/components/app.component.css'],

})
export class AppComponent {
  title = 'Test Driven Development with Angular';
}
