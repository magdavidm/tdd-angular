import { provideRouter, RouterConfig } from "@angular/router";

import { DashboardComponent } from '../components/dashboard.component';
import { ViewerComponent } from '../components/viewer.component';

export const routes: RouterConfig = [
  {
    component: DashboardComponent,
    path: "",
  },
  {
    component: DashboardComponent,
    path: "dashboard",
  },
  {
    component: ViewerComponent,
    path: "viewer",
  },
];

export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes),
];
