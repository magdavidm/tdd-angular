const concat = require('gulp-concat');
const del = require('del');
const gulp = require('gulp');
const plumber = require('gulp-plumber');
const runSequence = require('run-sequence');
const sourcemaps = require('gulp-sourcemaps');
const sysBuilder = require('systemjs-builder');
const tsc = require('gulp-typescript');
const uglify = require('gulp-uglify');
const exec = require('child_process').exec;

const tscConfig = require('./tsconfig.json');

// Clean the public directory
gulp.task('clean:public', function () {
  return del('public/*');
});

// Compile TypeScript to JS
gulp.task('compile:ts', function () {
  return gulp
    .src(tscConfig.filesGlob)
    .pipe(plumber({
      errorHandler: function (err) {
        console.error('>>> [tsc] Typescript compilation failed'.bold.green);
        this.emit('end');
      }}))
    .pipe(sourcemaps.init())
    .pipe(tsc(tscConfig.compilerOptions))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('public/dist'));
});

// Compile TypeScript to JS
gulp.task('compile:tsgeneric', function () {
  return tsProject
    .src()
    .pipe(tsc(tsProject.files))
    .js.pipe(gulp.dest(function(file){
      return file.base;
    })
    );
});

// Generate systemjs-based builds
gulp.task('bundle:js', function() {
  var builder = new sysBuilder('public', './system.config.js');
  return builder.buildStatic('app', 'public/dist/js/app.min.js')
    .then(function () {
      return del(['public/dist/js/**/*', '!public/dist/js/app.min.js']);
    })
    .catch(function(err) {
      console.error('>>> [systemjs-builder] Bundling failed'.bold.green, err);
    });
});

// Copy dependencies
gulp.task('copy:libs', function() {
  gulp.src(['node_modules/rxjs/**/*'])
    .pipe(gulp.dest('public/lib/js/rxjs'));

  gulp.src(['node_modules/angular2-in-memory-web-api/**/*'])
    .pipe(gulp.dest('public/lib/js/angular2-in-memory-web-api'));

  // concatenate non-angular2 libs, shims & systemjs-config
  gulp.src([
    // 'node_modules/jquery/dist/jquery.min.js',
    // 'node_modules/bootstrap/dist/js/bootstrap.min.js',
    'node_modules/es6-shim/es6-shim.min.js',
    'node_modules/es6-promise/dist/es6-promise.min.js',
    'node_modules/zone.js/dist/zone.js',
    'node_modules/reflect-metadata/Reflect.js',
    // 'node_modules/systemjs/dist/system-polyfills.js',
    // 'node_modules/chart.js/dist/chart.min.js',
    'node_modules/systemjs/dist/system.src.js',
    'system.config.js',
  ])
    .pipe(concat('vendors.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('public/lib/js'));

  // copy source maps
  gulp.src([
    'node_modules/es6-shim/es6-shim.map',
    'node_modules/reflect-metadata/Reflect.js.map',
    'node_modules/systemjs/dist/system-polyfills.js.map'
  ]).pipe(gulp.dest('public/lib/js'));

  gulp.src([
    'node_modules/bootstrap/dist/css/bootstrap.*'
  ]).pipe(gulp.dest('public/lib/css'));

  gulp.src([
    'node_modules/ng2-charts/**/*'
  ]).pipe(gulp.dest('public/lib/js/ng2-charts'));

  return gulp.src(['node_modules/@angular/**/*'])
    .pipe(gulp.dest('public/lib/js/@angular'));
});

// Minify JS bundle
gulp.task('minify:js', function() {
  return gulp
    .src('public/dist/js/app.min.js')
    .pipe(uglify())
    .pipe(gulp.dest('public/dist/js'));
});

gulp.task('copy:html', function(){
  return gulp.src([
    'src/*.html'
  ])
    .pipe(gulp.dest('public/'))
});

gulp.task('scripts', function(callback){
    runSequence(['copy:libs'], 'compile:ts', 'bundle:js', 'minify:js', callback);
});
gulp.task('scripts:dev', function(callback){
    runSequence(['copy:libs'], 'compile:ts', 'bundle:js', callback);
});

//Tests
gulp.task('test:e2e', function(callback){
  exec('protractor e2eTest/conf.js', function(err, stdout, stderr){
      console.log(stdout);
      console.log(stderr);
      callback(err);
  });
});

gulp.task('build', function(callback){
    runSequence('scripts', 'copy:html');
});

gulp.task('build:dev', function(callback){
    runSequence('scripts:dev', 'copy:html');
});

gulp.task('default', function(callback){
    runSequence('build', callback);
});