describe('viewer route check', function() {
  it('should check title of home page', function() {
    browser.get('http://127.0.0.1:8080/');
    expect(element(by.tagName('h2')).getText()).toBe('Dashboard')
    
    element(by.linkText('Viewer')).click();
    browser.sleep(1000);

    expect(element(by.tagName('h2')).getText()).toBe('Viewer')
    browser.sleep(1000);
  });
});