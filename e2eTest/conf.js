exports.config = {
  seleniumAddress: 'http://localhost:4444/wd/hub',
  framework: 'jasmine',
//   specs: ['todo2-spec.js'],
  specs: ['homepage-spec.js', 'viewer-route-spec.js', 'init-at-viewer-spec.js'],
  jasmineNodeOpts: {
      // isVerbose: false,
      // includeStackTrace: false,
      // print: function() {},
      // displayStacktrace: 'none'
  },
  useAllAngular2AppRoots: true
};