//"use strict";
// let System = require("systemjs");

let path = require('path');
var express = require("express");
var app = express();
var rootPath = path.normalize(__dirname);

app.set("port", (process.env.PORT || 8080));

// for static files
app.use('', express.static(rootPath + "/public"));

// anything else, send the NG2 app
app.get("*", function(req, res) {
//   console.log("sending application");
  res.sendFile(rootPath + "/public/index.html");
});

app.listen(app.get("port"), function() {
  console.log("Node app is running on port", app.get("port"));
});
